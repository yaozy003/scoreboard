﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tournaments.Extend;
using Tournaments.Models;
using Tournaments.Models.InfoModels;
using Tournaments.Services;

namespace Tournaments
{
    public partial class frmRank : Form
    {
        public frmRank()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
        }

        RegistrationModel registration = new RegistrationModel();
        ApiService apiService = new ApiService();
        /// <summary>
        /// Rank
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmRank_Load(object sender, EventArgs e)
        {
            try
            {
                //read bak file from local and pull previous rank data out.
                var result = FileHelper.ReadContent("registration.bak");

                //if .bak file is empty,get a message box.
                if (string.IsNullOrEmpty(result))
                {
                    MessageBox.Show("Tournament id is null");
                    return;
                }

                //form result into  Registeration Model
                registration = result.ToObject<RegistrationModel>();

                //check null for registration
                if (registration == null || string.IsNullOrEmpty(registration.TouranmentId))
                {
                    MessageBox.Show("Tournament Id is null. Please try again.");
                    return;
                }
                QualifyingModel qualifying = apiService.QueryMatches(registration.TouranmentId, MatchesTypeEnum.qualifying) as QualifyingModel;


                //check null for qualifying
                if(qualifying == null || qualifying.Rounds == null || qualifying.Rounds.Count == 0)
                {
                    MessageBox.Show("No data.");
                    return;
                }

                //create a ILIST for teams.
                IList<TeamModel> teams = new List<TeamModel>();



                //load data from qualifying model that links API
                foreach(var round in qualifying.Rounds)
                {
                    if (round.Result.Equals("-1"))
                    {
                        var r1 = teams.SingleOrDefault(x => x.Id.Equals(round.Teams[0].Id));
                        if (r1 == null)
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[0].DeepClone();
                            model.TotalPoints = "1";
                            teams.Add(model);
                        }
                        else
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[0].DeepClone();
                            r1.Score =( int.Parse(r1.Score) + int.Parse(model.Score)).ToString();
                            r1.TotalPoints = (int.Parse(r1.TotalPoints) + 1).ToString();
                        }

                        var r2 = teams.SingleOrDefault(x => x.Id.Equals(round.Teams[1].Id));
                        if (r2 == null)
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[1].DeepClone();
                            model.TotalPoints = "1";
                            teams.Add(model);
                        }
                        else
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[1].DeepClone();
                            r2.Score = (int.Parse(r1.Score) + int.Parse(model.Score)).ToString();
                            r2.TotalPoints = (int.Parse(r1.TotalPoints) + 1).ToString();
                        }
                    }
                    else if(round.Result.Equals("-2"))
                    {
                        continue;
                    }
                    else
                    {
                        var r1 = teams.SingleOrDefault(x => x.Id.Equals(round.Teams[0].Id));
                        if (r1 == null)
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[0].DeepClone();
                            model.TotalPoints = round.Result.Equals(model.Id)?"2":"0";
                            teams.Add(model);
                        }
                        else
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[0].DeepClone();
                            r1.Score = (int.Parse(r1.Score) + int.Parse(model.Score)).ToString();
                            r1.TotalPoints = (int.Parse(r1.TotalPoints) + (round.Result.Equals(model.Id) ? 2: 0)).ToString();
                        }

                        var r2 = teams.SingleOrDefault(x => x.Id.Equals(round.Teams[1].Id));
                        if (r2 == null)
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[1].DeepClone();
                            model.TotalPoints = round.Result.Equals(model.Id) ? "2" : "0";
                            teams.Add(model);
                        }
                        else
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[1].DeepClone();
                            r2.Score = (int.Parse(r1.Score) + int.Parse(model.Score)).ToString();
                            r2.TotalPoints = (int.Parse(r1.TotalPoints) + (round.Result.Equals(model.Id) ? 2 : 0)).ToString();
                        }
                    }
                }

                
                IList<TeamModel> orderTeams = new List<TeamModel>();
                var orders = from t in teams
                             orderby t.TotalPoints descending, t.Score descending
                             select t;

                foreach(var o in orders)
                {
                    orderTeams.Add(o);
                }

                //create a datable
                DataTable datas = new DataTable();
                DataColumn teamId = new DataColumn("Team Id", typeof(string));
                DataColumn teamName = new DataColumn("Team Name", typeof(string));
                DataColumn teamScore = new DataColumn("Team Score", typeof(int));
                DataColumn teamTP = new DataColumn("Total Points", typeof(int));
                datas.Columns.Add(teamId);
                datas.Columns.Add(teamName);
                datas.Columns.Add(teamScore);
                datas.Columns.Add(teamTP);
                
                foreach(var x in orderTeams)
                {
                    DataRow row = datas.NewRow();
                    row[0] = x.Id;
                    row[1] = x.Name;
                    row[2] = x.TotalPoints;
                    row[3] = x.Score;

                    datas.Rows.Add(row.ItemArray);
                }

                dgvRank.DataSource = datas;
                dgvRank.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dgvRank.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
