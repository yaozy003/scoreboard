﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Services
{
    public class FileHelper
    {
        private static string filePath = AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// Save tournament's id .
        /// </summary>
        /// <param name="tournamentId"></param>
        /// <returns></returns>
        public static bool SaveContent(string content,string fileName)
        {
            try
            {
                string path = filePath + fileName;
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                using(FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    using(StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine(content);
                        sw.Flush();
                        sw.Close();
                    }
                }

                return true;
            }
            catch (FileNotFoundException)
            {
                throw;
            }
            catch (UnauthorizedAccessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Read tournament's id if the file is existing .
        /// </summary>
        /// <returns></returns>
        public static string ReadContent(string fileName)
        {
            try
            {
                string path = filePath + fileName;
                string result = string.Empty;
                if (!File.Exists(path))
                {
                    return string.Empty;
                }

                using(FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using(StreamReader sr = new StreamReader(fs))
                    {
                        result = sr.ReadLine();
                    }
                }

                return result;
            }
            catch (FileNotFoundException)
            {
                throw;
            }
            catch (UnauthorizedAccessException)
            {
                throw;
            }
            catch (FileLoadException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
