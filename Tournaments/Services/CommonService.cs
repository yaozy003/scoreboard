﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournaments.Extend;
using Tournaments.Models;
using Tournaments.Models.InfoModels;

namespace Tournaments.Services
{
    public class CommonService
    {

        private const string registrationFile = "registration.bak";
        private const string scoreFile = "score.bak";

        ApiService apiService = new ApiService();
        public IList<RoundModel> GenerateQualMatches(string tournamentId)
        {
            IList<RoundModel> rounds = new List<RoundModel>();
            int roundIndex = 1;
            try
            {
                TeamsModel matches = apiService.QueryTeams(tournamentId);
                for (int i = 0; i < matches.Teams.Count - 1; i++)
                {
                    for (int j = i + 1; j < matches.Teams.Count; j++)
                    {
                        RoundModel model = new RoundModel();
                        model.Round = roundIndex.ToString();
                        model.Teams.Add(matches.Teams[i].DeepClone());
                        model.Teams.Add(matches.Teams[j].DeepClone());

                        rounds.Add(model);
                        roundIndex++;
                    }
                }

                return rounds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Registration content from file
        /// </summary>
        /// <returns></returns>
        public RegistrationModel GetRegistration()
        {
            try
            {
                RegistrationModel model = new RegistrationModel();
                string r = FileHelper.ReadContent(registrationFile);
                if (string.IsNullOrEmpty(r))
                {
                    return null;
                }
                model = r.ToObject<RegistrationModel>();

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ScoreModel GetScore()
        {
            try
            {
                ScoreModel model = new ScoreModel();
                string s = FileHelper.ReadContent(scoreFile);
                if (string.IsNullOrEmpty(s))
                {
                    return null;
                }
                model = s.ToObject<ScoreModel>();
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Query qualifying matches, generate semifinals 
        /// </summary>
        /// <returns></returns>
        public IList<RoundModel> GenerateSemiFinals(string tournamentId)
        {
            try
            {
                IList<RoundModel> finals = new List<RoundModel>();

                IList<TeamModel> teams = GetTeamsRank(tournamentId);
                IList<TeamModel> orderTeams = new List<TeamModel>();
                if(teams == null || teams.Count < 4)
                {
                    return null;
                }

                var orders = from t in teams
                             orderby t.TotalPoints descending, t.Score descending
                             select t;

                foreach(var o in orders)
                {
                    orderTeams.Add(o);
                }

                RoundModel firstModel = new RoundModel();
                firstModel.Round = "1";
                firstModel.Teams.Add(orderTeams[0]);
                firstModel.Teams.Add(orderTeams[3]);
                finals.Add(firstModel);

                RoundModel secondModel = new RoundModel();
                secondModel.Round = "2";
                secondModel.Teams.Add(orderTeams[1]);
                secondModel.Teams.Add(orderTeams[2]);
                finals.Add(secondModel);

                return finals;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<RoundModel> GenerateFinals(string tournamentId)
        {
            try
            {
                IList<RoundModel> empty = new List<RoundModel>();
                IList<RoundModel> finals = new List<RoundModel>();
                FinalsModel tmp = apiService.QueryMatches(tournamentId, MatchesTypeEnum.finals) as FinalsModel;

                if(tmp == null || tmp.Rounds == null || tmp.Rounds.Count == 0)
                {
                    return empty;
                }
                else
                {

                    finals.Add(tmp.Rounds[0]);
                    finals.Add(tmp.Rounds[1]);

                    RoundModel firstModel = new RoundModel();
                    var r = tmp.Rounds[0].Teams.SingleOrDefault(s => s.Id.Equals(tmp.Rounds[0].Result));
                    var r1 = tmp.Rounds[1].Teams.SingleOrDefault(s => s.Id.Equals(tmp.Rounds[1].Result));
                    if (r != null && r1 != null)
                    {
                        firstModel.Round = "3";
                        firstModel.Teams.Add(r.DeepClone());
                        firstModel.Teams.Add(r1.DeepClone());
                        finals.Add(firstModel);
                    }

                    var r3 = tmp.Rounds[0].Teams.SingleOrDefault(s => !s.Id.Equals(tmp.Rounds[0].Result));
                    var r4 = tmp.Rounds[1].Teams.SingleOrDefault(s => !s.Id.Equals(tmp.Rounds[1].Result));
                    RoundModel secondModel = new RoundModel();
                    
                    if (r3 != null && r4 != null)
                    {
                        secondModel.Round = "4";
                        secondModel.Teams.Add(r3.DeepClone());
                        secondModel.Teams.Add(r4.DeepClone());
                        finals.Add(secondModel);
                    }
                    return finals;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<TeamModel> GetTeamsRank(string tournamentId)
        {
            try
            {
                QualifyingModel qualifying = apiService.QueryMatches(tournamentId, MatchesTypeEnum.qualifying) as QualifyingModel;

                if (qualifying == null || qualifying.Rounds == null || qualifying.Rounds.Count == 0)
                {
                    return null;
                }

                IList<TeamModel> teams = new List<TeamModel>();

                foreach (var round in qualifying.Rounds)
                {
                    if (round.Result.Equals("-1"))
                    {
                        var r1 = teams.SingleOrDefault(x => x.Id.Equals(round.Teams[0].Id));
                        if (r1 == null)
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[0].DeepClone();
                            model.TotalPoints = "1";
                            teams.Add(model);
                        }
                        else
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[0].DeepClone();
                            r1.Score = (int.Parse(r1.Score) + int.Parse(model.Score)).ToString();
                            r1.TotalPoints = (int.Parse(r1.TotalPoints) + 1).ToString();
                        }

                        var r2 = teams.SingleOrDefault(x => x.Id.Equals(round.Teams[1].Id));
                        if (r2 == null)
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[1].DeepClone();
                            model.TotalPoints = "1";
                            teams.Add(model);
                        }
                        else
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[1].DeepClone();
                            r2.Score = (int.Parse(r1.Score) + int.Parse(model.Score)).ToString();
                            r2.TotalPoints = (int.Parse(r1.TotalPoints) + 1).ToString();
                        }
                    }
                    else if (round.Result.Equals("-2"))
                    {
                        continue;
                    }
                    else
                    {
                        var r1 = teams.SingleOrDefault(x => x.Id.Equals(round.Teams[0].Id));
                        if (r1 == null)
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[0].DeepClone();
                            model.TotalPoints = round.Result.Equals(model.Id) ? "2" : "0";
                            teams.Add(model);
                        }
                        else
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[0].DeepClone();
                            r1.Score = (int.Parse(r1.Score) + int.Parse(model.Score)).ToString();
                            r1.TotalPoints = (int.Parse(r1.TotalPoints) + (round.Result.Equals(model.Id) ? 2 : 0)).ToString();
                        }

                        var r2 = teams.SingleOrDefault(x => x.Id.Equals(round.Teams[1].Id));
                        if (r2 == null)
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[1].DeepClone();
                            model.TotalPoints = round.Result.Equals(model.Id) ? "2" : "0";
                            teams.Add(model);
                        }
                        else
                        {
                            TeamModel model = new TeamModel();
                            model = round.Teams[1].DeepClone();
                            r2.Score = (int.Parse(r1.Score) + int.Parse(model.Score)).ToString();
                            r2.TotalPoints = (int.Parse(r1.TotalPoints) + (round.Result.Equals(model.Id) ? 2 : 0)).ToString();
                        }
                    }
                }

                return teams;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<string> GetQualIsOver (string tournamentId)
        {
            try
            {
                IList<string> empty = new List<string>();
                QualifyingModel qualifying = apiService.QueryMatches(tournamentId, MatchesTypeEnum.qualifying) as QualifyingModel;

                if (qualifying == null || qualifying.Rounds == null || qualifying.Rounds.Count == 0)
                {
                    return empty;
                }

                IList<string> qualIsOver = new List<string>();

                foreach (var round in qualifying.Rounds)
                {
                    if (round.Result != "-2")
                    {
                        qualIsOver.Add(round.Round);
                    }
                }
                return qualIsOver;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Get the final's match number which is over.
        /// </summary>
        /// <param name="tournamentId"></param>
        /// <returns></returns>
        public IList<string> GetFinalIsOver(string tournamentId)
        {
            try
            {
                IList<string> empty = new List<string>();
                IList<string> finalIsOver = new List<string>();
                FinalsModel finalsModel = apiService.QueryMatches(tournamentId, MatchesTypeEnum.finals) as FinalsModel;
                if(finalsModel == null || finalsModel.Rounds == null || finalsModel.Rounds.Count == 0)
                {
                    return empty;
                }

                foreach(var f in finalsModel.Rounds)
                {
                    if(!f.Result.Equals("-2"))
                    {
                        finalIsOver.Add(f.Round);
                    }
                }
                return finalIsOver;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
