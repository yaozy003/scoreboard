﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournaments.Api;
using Tournaments.Extend;
using Tournaments.Models;
using Tournaments.Models.ApiModels;

namespace Tournaments.Services
{
    public class ApiService
    {
        RequestHelper requestHelper = new RequestHelper();
        private const string baseUri = @"http://partiklezoo.com/aerialassist/index.php?";

        /// <summary>
        /// Add new tournament, return new TournamentModel
        /// </summary>
        /// <param name="tournament"></param>
        /// <returns></returns>
        public TournamentModel AddTouranment(string tournament)
        {
            try
            {
                StringBuilder uri = new StringBuilder();
                uri.Append(baseUri);
                uri.Append($"action=add&type=tournament&name={tournament}");

                string result = requestHelper.CallRpc(uri.ToString());

                var r = result.ToObject<TournamentJsonModel>();
                if (r.Result.Equals(MsgEnum.success.ToString()))
                {
                    TournamentModel model = new TournamentModel();
                    model.Id = r.Id;
                    model.Name = tournament;
                    return model;
                }
                else
                {
                    throw new Exception($"Regitering touranment {tournament} false.");
                }
                
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// When Add new Team, return the new TeamModel
        /// </summary>
        /// <param name="tournamentId"></param>
        /// <param name="teamName"></param>
        /// <returns></returns>
        public TeamModel AddTeam(string tournamentId, string teamName)
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(baseUri);
                builder.Append($"action=add&type=team&tournament={tournamentId}&name={teamName}");
                string result = requestHelper.CallRpc(builder.ToString());
                var r = result.ToObject<TeamJsonModel>();

                if (r.Result.Equals(MsgEnum.success.ToString()))
                {
                    TeamModel model = new TeamModel();
                    model.Id = r.Id;
                    model.Name = r.Name;
                    model.TournamentId = tournamentId;
                    return model;
                }
                else
                {
                    throw new Exception("Add teams false.");
                }                               
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// add a player into a team
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="playerName"></param>
        /// <returns></returns>
        public PlayerModel AddPlayer(string teamId,string playerName)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(baseUri);
                sb.Append($"action=add&type=player&team={teamId}&name={playerName}");
                string result = requestHelper.CallRpc(sb.ToString());

                var r = result.ToObject<PlayerJsonModel>();
                if (r.Result.Equals(MsgEnum.success.ToString()))
                {
                    PlayerModel player = new PlayerModel();
                    player.TeamId = teamId;
                    player.Name = playerName;
                    player.Id = r.Id;

                    return player;
                }
                else
                {
                    throw new Exception("Add player false.");
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Add a team to a match(not needed) or Update a score
        /// </summary>
        /// <param name="tournamentId"></param>
        /// <param name="teamId"></param>
        /// <param name="macthtype"></param>
        /// <param name="matchNumber"></param>
        /// <param name="score"></param>
        public bool UpdateMatch(string tournamentId,string teamId,MatchTypeEnum matchType,int matchNumber,int score = -1)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(baseUri);
                if(score < 0)
                {
                    sb.Append($"action=add&type={TypeEnum.matchresult.ToString()}&tournament={tournamentId}&team={teamId}&matchtype={matchType.ToString()}&matchnumber={matchNumber}");
                }
                else
                {
                    sb.Append($"action=add&type={TypeEnum.matchresult.ToString()}&tournament={tournamentId}&team={teamId}&matchtype={matchType.ToString()}&matchnumber={matchNumber}&score={score}");
                }
                
                var result = requestHelper.CallRpc(sb.ToString());

                MatchJsonModel model = result.ToObject<MatchJsonModel>();
                if (model.Result.Equals(MsgEnum.success.ToString()))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// query the teams
        /// </summary>
        /// <param name="tournamentId"></param>
        /// <returns></returns>
        public TeamsModel QueryTeams(string tournamentId)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(baseUri);
                sb.Append($"action=teams&tournament={tournamentId}&format=json");

                string result = requestHelper.CallRpc(sb.ToString());

                TeamsModel teams = result.ToObject<TeamsModel>();

                return teams;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieving List of Players per Team
        /// </summary>
        /// <param name="teamId"></param>
        /// <returns></returns>
        public PlayersModel QueryPlayers(string teamId)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(baseUri);
                sb.Append($"action=players&team={teamId}&format=json");

                string result = requestHelper.CallRpc(sb.ToString());
                PlayersModel players = result.ToObject<PlayersModel>();

                return players;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieving List of Matches per Tournament
        /// </summary>
        /// <param name="tournamentId"></param>
        /// <param name="actionType"></param>
        /// <returns></returns>
        public BaseMatches QueryMatches(string tournamentId,MatchesTypeEnum actionType)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(baseUri);
                sb.Append($"action={actionType.ToString()}&tournament={tournamentId}&format=json");

                string result = requestHelper.CallRpc(sb.ToString());

                

                if (actionType == MatchesTypeEnum.qualifying)
                {
                    QualifyingModel model = new QualifyingModel();
                    model = result.ToObject<QualifyingModel>();
                    return model;
                }
                else
                {
                    FinalsModel model = new FinalsModel();
                    model = result.ToObject<FinalsModel>();
                    return model;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
