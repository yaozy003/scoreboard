﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;

namespace Tournaments.Api
{
    public class RequestHelper
    {
        
        /// <summary>
        /// The common method which is used to call the Api.
        /// </summary>
        /// <param name="url">the uri that includes params.</param>
        /// <returns></returns>
        public string CallRpc(string url)
        {
            try
            {
                Uri uri = new Uri(url);
                WebRequest client = WebRequest.Create(url);
                WebResponse response =  client.GetResponse();
                string result = (new StreamReader(response.GetResponseStream())).ReadToEnd() ;
                return result;
            }catch(ArgumentNullException)
            {
                throw;
            }
            catch (HttpRequestException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
