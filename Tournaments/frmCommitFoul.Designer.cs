﻿namespace Tournaments
{
    partial class frmCommitFoul
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbYellowCard = new System.Windows.Forms.RadioButton();
            this.rbRedCard = new System.Windows.Forms.RadioButton();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtPlayerId = new System.Windows.Forms.TextBox();
            this.txtPlayerName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Player Id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Player Name:";
            // 
            // rbYellowCard
            // 
            this.rbYellowCard.AutoSize = true;
            this.rbYellowCard.Location = new System.Drawing.Point(61, 145);
            this.rbYellowCard.Name = "rbYellowCard";
            this.rbYellowCard.Size = new System.Drawing.Size(119, 25);
            this.rbYellowCard.TabIndex = 2;
            this.rbYellowCard.TabStop = true;
            this.rbYellowCard.Text = "Yellow Card";
            this.rbYellowCard.UseVisualStyleBackColor = true;
            // 
            // rbRedCard
            // 
            this.rbRedCard.AutoSize = true;
            this.rbRedCard.Location = new System.Drawing.Point(254, 145);
            this.rbRedCard.Name = "rbRedCard";
            this.rbRedCard.Size = new System.Drawing.Size(98, 25);
            this.rbRedCard.TabIndex = 3;
            this.rbRedCard.TabStop = true;
            this.rbRedCard.Text = "Red Card";
            this.rbRedCard.UseVisualStyleBackColor = true;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(159, 201);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(107, 35);
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtPlayerId
            // 
            this.txtPlayerId.Location = new System.Drawing.Point(174, 21);
            this.txtPlayerId.Name = "txtPlayerId";
            this.txtPlayerId.ReadOnly = true;
            this.txtPlayerId.Size = new System.Drawing.Size(254, 29);
            this.txtPlayerId.TabIndex = 5;
            // 
            // txtPlayerName
            // 
            this.txtPlayerName.Location = new System.Drawing.Point(174, 78);
            this.txtPlayerName.Name = "txtPlayerName";
            this.txtPlayerName.ReadOnly = true;
            this.txtPlayerName.Size = new System.Drawing.Size(254, 29);
            this.txtPlayerName.TabIndex = 6;
            // 
            // frmCommitFoul
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 274);
            this.Controls.Add(this.txtPlayerName);
            this.Controls.Add(this.txtPlayerId);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.rbRedCard);
            this.Controls.Add(this.rbYellowCard);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frmCommitFoul";
            this.Text = "Setting Foul";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbYellowCard;
        private System.Windows.Forms.RadioButton rbRedCard;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtPlayerId;
        private System.Windows.Forms.TextBox txtPlayerName;
    }
}