﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tournaments.Extend;
using Tournaments.Models;
using Tournaments.Models.InfoModels;
using Tournaments.Services;

namespace Tournaments
{
    public partial class frmRegistration : Form
    {
        public frmRegistration()
        {
            InitializeComponent();
        }

        private const int TEAM_COUNT = 6;
        private const int PLAYERS_COUNT = 5;
        private string fileName = "registration.bak";
        ApiService apiService = new ApiService();
        RegistrationModel registrationModel = new RegistrationModel();
        TournamentModel tournament = new TournamentModel();
        IList<TeamModel> teams = new List<TeamModel>();
        IList<PlayerModel> players = new List<PlayerModel>();

        /// <summary>
        /// Register tournament.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetTournament_Click(object sender, EventArgs e)
        {
            try
            {
                btnSetTournament.Enabled = false;

                string tournamentName = string.Empty;
                tournamentName = txtTournament.Text.Trim();
                if (string.IsNullOrEmpty(tournamentName))
                {
                    MessageBox.Show("Tournament's name is empty.", "Tips");
                    return;
                }

                tournament = apiService.AddTouranment(tournamentName);

                // register new tournament, clear all teams and all players. 
                rtbTeams.Clear();
                rtbPlayers.Clear();
                teams.Clear();
                players.Clear();
                txtTournament.Text = string.Empty;
                labelTournament.Text = $"Tournament Id: {tournament.Id}; Name {tournament.Name}";

                // Save touranment id.
                

                MessageBox.Show("Register new tournament successfully.");
                btnAddTeams.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            finally
            {
                registrationModel.TouranmentId = tournament.Id;
                registrationModel.TouranmentName = tournament.Name;
                FileHelper.SaveContent(registrationModel.ConvertToString(),fileName);
                btnSetTournament.Enabled = true;
            }
        }

        
        /// <summary>
        /// Add teams for this tournament
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddTeams_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tournament.Id))
                {
                    MessageBox.Show("Please registering tournament first.");
                    return;
                }

                btnAddTeams.Enabled = false;
                
                string teamName = string.Empty;

                teamName = txtTeamName.Text.Trim();
                if (string.IsNullOrEmpty(teamName))
                {
                    MessageBox.Show("Team's name is empty.", "Tip");
                    return;
                }

                if(teams.Count == TEAM_COUNT)
                {
                    MessageBox.Show($"Only add {TEAM_COUNT} teams");
                    return;
                }

                TeamModel model = new TeamModel();
                model = apiService.AddTeam(tournament.Id, teamName);
                teams.Add(model);

                DisplayTeams(teams);

                cbTeams.DataSource = null;
                cbTeams.DataSource = teams;
                cbTeams.DisplayMember = "Name";
                cbTeams.ValueMember = "Id";
                cbTeams.SelectedIndex = 0;

                txtTeamName.Text = string.Empty;              

            }catch(Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            finally
            {
                btnAddTeams.Enabled = true;
                txtTeamName.Focus();
                registrationModel.Teams = teams;
                FileHelper.SaveContent(registrationModel.ConvertToString(),fileName);
            }
        }

        /// <summary>
        /// Adding players.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddPlayer_Click(object sender, EventArgs e)
        {
            try
            {
                btnAddPlayer.Enabled = false;
                if(teams.Count == 0)
                {
                    MessageBox.Show("Please add teams first!");
                    return;
                }

                TeamModel selected = cbTeams.SelectedItem as TeamModel;
                IList<PlayerModel> selectedPlayers = players.Where(x => x.TeamId.Equals(selected.Id)).ToList();

                string playerName = string.Empty;
                playerName = txtPlayerName.Text.Trim();
                if (string.IsNullOrEmpty(playerName))
                {
                    MessageBox.Show("Player name is null !");
                    return;
                }

                if(selectedPlayers.Count == PLAYERS_COUNT)
                {
                    MessageBox.Show($"Only add {PLAYERS_COUNT} players.");
                    return;
                }

                PlayerModel model = apiService.AddPlayer(selected.Id, playerName);
                if(model == null)
                {
                    MessageBox.Show("Too many players, can't insert !");
                    return;
                }
                players.Add(model);


                selectedPlayers = players.Where(x => x.TeamId.Equals(selected.Id)).ToList();
                DisplayPlayers(selectedPlayers);
                txtPlayerName.Text = string.Empty;               
            }catch(Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            finally
            {
                btnAddPlayer.Enabled = true;
                txtPlayerName.Focus();
                registrationModel.Players = players;
                FileHelper.SaveContent(registrationModel.ConvertToString(),fileName);
            }
        }

        /// <summary>
        /// Teams' index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbTeams_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtPlayerName.Text = string.Empty;
                rtbPlayers.Clear();
                txtPlayerName.Focus();

                TeamModel selected = cbTeams.SelectedItem as TeamModel;
                if(registrationModel.Players == null || registrationModel.Players.Count == 0)
                {
                    return;
                }
                IList<PlayerModel> selectedPlayers = players.Where(x => x.TeamId.Equals(selected.Id)).ToList();                
                if(selectedPlayers == null | selectedPlayers.Count == 0)
                {
                    return;
                }
                DisplayPlayers(selectedPlayers);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }


        private void DisplayPlayers(IList<PlayerModel> list, bool clear = true)
        {
            
            if (clear)
            {
                rtbPlayers.Clear();
                rtbPlayers.Text = string.Empty;
            }
            if (list == null || list.Count == 0)
            {
                return;
            }
                foreach (var x in list)
            {
                rtbPlayers.AppendText($"Player Id: {x.Id}; Player Name: {x.Name}\n");
            }
        }

        private void DisplayTeams(IList<TeamModel> list,bool clear = true)
        {
            if (clear)
            {
                rtbTeams.Clear();
                rtbTeams.Text = string.Empty;
            }
            foreach(var team in list)
            {
                rtbTeams.AppendText($"Team Id: {team.Id}; Team Name: {team.Name}\n");
            }
        }

        /// <summary>
        /// When open the window, loading bak file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmRegistration_Load(object sender, EventArgs e)
        {
            try
            {
                string bak = FileHelper.ReadContent(fileName);
                if (string.IsNullOrEmpty(bak))
                {
                    DialogResult r = MessageBox.Show("The bak file is not exists.\nContinue?", "Tip",MessageBoxButtons.OKCancel);
                    if(r == DialogResult.Cancel)
                    {
                        this.Close();
                    }
                }
                else
                {
                    registrationModel = bak.ToObject<RegistrationModel>();

                    if (!string.IsNullOrEmpty(registrationModel.TouranmentId))
                    {
                        DialogResult r = MessageBox.Show("Using the bak file ?", "Tip", MessageBoxButtons.OKCancel);
                        if (r == DialogResult.OK)
                        {

                            tournament.Id = registrationModel.TouranmentId;
                            tournament.Name = registrationModel.TouranmentName;
                            labelTournament.Text = $"Tournament Id: {tournament.Id}; Name {tournament.Name}";
                            if (registrationModel.Teams != null && registrationModel.Teams.Count != 0)
                            {
                                teams = registrationModel.Teams;
                                cbTeams.DataSource = teams;
                                cbTeams.DisplayMember = "Name";
                                cbTeams.ValueMember = "Id";
                                cbTeams.SelectedIndex = 0;
                                DisplayTeams(teams);
                            }
                            if(registrationModel.Players != null && registrationModel.Players.Count != 0)
                            {
                                players = registrationModel.Players;
                            }
                        }
                        else
                        {
                            registrationModel = new RegistrationModel();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }
    }
}
