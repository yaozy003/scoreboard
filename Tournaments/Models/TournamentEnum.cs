﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public enum MatchTypeEnum
    {
        qual = 0,
        final = 1
    }

    public enum TypeEnum
    {
        tournament = 0,
        team = 1,
        player = 2,
        matchresult = 3
    }

    /// <summary>
    /// The action type which is used to retrieve list of matches per tournament
    /// </summary>
    public enum MatchesTypeEnum
    {
        qualifying = 0,
        finals = 1
    }

    /// <summary>
    /// -2 for no socres are entered
    /// -1 for a tie
    /// </summary>
    public enum MatchesResultEnum
    {
        noScore = -2,
        tie = -1
    }

    public enum MsgEnum
    {
        success = 0
    }

    public enum FinalsType
    {
        semiFinal = 0,
        final = 1
    }

    public enum ScoreMatchTypeEnum
    {
        qual = 0,
        semiFinal = 1,
        final = 2
    }
}
