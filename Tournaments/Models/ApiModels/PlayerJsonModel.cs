﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models.ApiModels
{
    public class PlayerJsonModel
    {
        //player result
        public string Result { get; set; }
        //player type
        public string Type { get; set; }
        // player id
        public string Id { get; set; }
        //player name
        public string Name { get; set; }
    }
}
