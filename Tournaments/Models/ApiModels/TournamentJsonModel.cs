﻿namespace Tournaments.Models.ApiModels
{
    public class TournamentJsonModel
    {
        // tournament result
        public string Result { get; set; }
        // tournamen type
        public string Type { get; set; }
        // tournamen id
        public string Id { get; set; }
    }
}
