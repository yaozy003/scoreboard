﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models.ApiModels
{
    public class MatchJsonModel
    {
        // Match Result
        public string Result { get; set; }
        public string Message { get; set; }
    }
}
