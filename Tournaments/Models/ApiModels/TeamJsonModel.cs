﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models.ApiModels
{
    public class TeamJsonModel
    {
        // Team Result
        public string Result { get; set; }
        //Team Type
        public string Type { get; set; }
        // Team Id
        public string Id { get; set;}
        // Team Name
        public string Name { get; set; }
    }
}
