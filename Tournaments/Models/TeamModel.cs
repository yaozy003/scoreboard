﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class TeamModel
    {
        public string TournamentId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Score { get; set; }
        public string TotalPoints { get; set; }
        public TeamModel DeepClone()
        {
            TeamModel model = new TeamModel();
            model.TournamentId = TournamentId;
            model.Id = Id;
            model.Name = Name;
            model.Score = Score;
            model.TotalPoints = TotalPoints;
            return model;
        }
    }
}
