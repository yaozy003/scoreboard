﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class PlayersModel
    {
        public IList<PlayerModel> Players { get; set; }
    }
}
