﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class FinalsModel:BaseMatches
    {
        [JsonProperty("finals")]
       public IList<RoundModel> Rounds { get; set; }
    }
}
