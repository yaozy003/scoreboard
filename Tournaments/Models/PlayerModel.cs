﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class PlayerModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string TeamId { get; set; }
        public int YellowCard { get; set; }
        public int RedCard { get; set; }
        public DateTime BeginRemoveTime { get; set; }
    }
}
