﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models.InfoModels
{
    /// <summary>
    /// this model is use to store 'frmBaseSetting' form register data.
    /// </summary>
    public class RegistrationModel
    {
        public string TouranmentId { get; set; }
        public string TouranmentName { get; set; }
        public IList<TeamModel> Teams { get; set; }
        public IList<PlayerModel> Players { get; set; }
    }
}
