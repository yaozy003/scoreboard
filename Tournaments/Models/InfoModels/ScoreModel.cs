﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models.InfoModels
{
    public class ScoreModel
    {
        private TournamentModel _touranment = new TournamentModel();
        private RoundModel _currentModel = new RoundModel();
        public TournamentModel Touranment
        {
            get { return _touranment; }
            set { _touranment = value; }
        }
        public RoundModel CurrentMatch
        {
            get { return _currentModel; }
            set { _currentModel = value; }
        }
        //team1  score
        public int Score1 { get; set; }
        //team2 score
        public int Score2 { get; set; }
        //team1 Bonus
        public int Bonus1 { get; set; }
        //team2 Bonus
        public int Bonus2 { get; set; }
        // current round
        public int CurrentRound { get; set; }
        // match number
        public string MatchNum { get; set; }
        // match type
        public MatchTypeEnum MatchType { get; set; }
        //score match type
        public ScoreMatchTypeEnum ScoreType { get; set; }
       
    }
}
