﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class RoundModel
    {
        private IList<TeamModel> _teams = new List<TeamModel>();
        private bool _roundEnd = false;

        public string Round { get; set; }
        public IList<TeamModel> Teams
        {
            get { return _teams; }
            set { this._teams = value; }
        }
        public string Result { get; set; }
        public bool RoundEnd
        {
            get { return _roundEnd; }
            set { _roundEnd = value; }
        }
    }
}
