﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class RecordModel
    {
        public string TournamentId { get; set; }
        /// <summary>
        /// If this program is closed normal, the property is true.
        /// </summary>
        public bool IsNormal { get; set; }
    }
}
