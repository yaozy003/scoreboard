﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class TournamentModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
