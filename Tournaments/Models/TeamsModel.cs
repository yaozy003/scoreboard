﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class TeamsModel
    {
        public IList<TeamModel> Teams { get; set; }
    }
}
