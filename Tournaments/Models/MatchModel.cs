﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournaments.Models
{
    public class MatchModel
    {
        public string TournamentId { get; set; }
        public string MatchNumber { get; set; }
        public TeamModel Team { get; set; }
        public int Score { get; set; }
        public MatchTypeEnum MatchType { get; set; }
        /// <summary>
        /// Identifier the state of this match
        /// </summary>
        public bool IsOver { get; set; }
    }
}
