﻿using System.Windows.Forms;

namespace Tournaments
{
    partial class frmScoring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBounsPoint2 = new System.Windows.Forms.Label();
            this.lblBounsPoint1 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Bouns = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.BtnIntercept = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.BtnBouns2 = new System.Windows.Forms.Button();
            this.BtnGoal2 = new System.Windows.Forms.Button();
            this.lblScore2 = new System.Windows.Forms.Label();
            this.lblScore1 = new System.Windows.Forms.Label();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvTeam2Players = new System.Windows.Forms.DataGridView();
            this.TeamName2 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvTeam1Players = new System.Windows.Forms.DataGridView();
            this.btnSaveScores = new System.Windows.Forms.Button();
            this.BtnGoal = new System.Windows.Forms.Button();
            this.BtnBouns = new System.Windows.Forms.Button();
            this.Label4 = new System.Windows.Forms.Label();
            this.lblMatchNumber = new System.Windows.Forms.Label();
            this.txtTeam2 = new System.Windows.Forms.TextBox();
            this.TeamName1 = new System.Windows.Forms.Label();
            this.txtTeam1 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvMatches = new System.Windows.Forms.DataGridView();
            this.lblMatchNum = new System.Windows.Forms.Label();
            this.btnSemiFinalMatch = new System.Windows.Forms.Button();
            this.btnFinals = new System.Windows.Forms.Button();
            this.lblRoundNum = new System.Windows.Forms.Label();
            this.cbRoundNum = new System.Windows.Forms.ComboBox();
            this.btnChangeRound = new System.Windows.Forms.Button();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeam2Players)).BeginInit();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeam1Players)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMatches)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBounsPoint2
            // 
            this.lblBounsPoint2.AutoSize = true;
            this.lblBounsPoint2.BackColor = System.Drawing.Color.Aquamarine;
            this.lblBounsPoint2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBounsPoint2.Location = new System.Drawing.Point(1019, 154);
            this.lblBounsPoint2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBounsPoint2.Name = "lblBounsPoint2";
            this.lblBounsPoint2.Size = new System.Drawing.Size(32, 36);
            this.lblBounsPoint2.TabIndex = 56;
            this.lblBounsPoint2.Text = "0";
            // 
            // lblBounsPoint1
            // 
            this.lblBounsPoint1.AutoSize = true;
            this.lblBounsPoint1.BackColor = System.Drawing.Color.Aquamarine;
            this.lblBounsPoint1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBounsPoint1.Location = new System.Drawing.Point(1019, 65);
            this.lblBounsPoint1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBounsPoint1.Name = "lblBounsPoint1";
            this.lblBounsPoint1.Size = new System.Drawing.Size(32, 36);
            this.lblBounsPoint1.TabIndex = 55;
            this.lblBounsPoint1.Text = "0";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(-21, -10);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(0, 15);
            this.Label1.TabIndex = 54;
            // 
            // Bouns
            // 
            this.Bouns.AutoSize = true;
            this.Bouns.Font = new System.Drawing.Font("SimSun", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Bouns.Location = new System.Drawing.Point(962, 28);
            this.Bouns.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Bouns.Name = "Bouns";
            this.Bouns.Size = new System.Drawing.Size(155, 25);
            this.Bouns.TabIndex = 52;
            this.Bouns.Text = "Bouns Point";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(613, 170);
            this.Label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(0, 15);
            this.Label6.TabIndex = 51;
            // 
            // BtnIntercept
            // 
            this.BtnIntercept.Font = new System.Drawing.Font("SimSun", 15F);
            this.BtnIntercept.Location = new System.Drawing.Point(1063, 524);
            this.BtnIntercept.Margin = new System.Windows.Forms.Padding(4);
            this.BtnIntercept.Name = "BtnIntercept";
            this.BtnIntercept.Size = new System.Drawing.Size(255, 38);
            this.BtnIntercept.TabIndex = 50;
            this.BtnIntercept.Text = "Intercept";
            this.BtnIntercept.UseVisualStyleBackColor = true;
            this.BtnIntercept.Click += new System.EventHandler(this.BtnIntercept_Click);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Rockwell", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(1162, 16);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(141, 42);
            this.Label2.TabIndex = 49;
            this.Label2.Text = "SCORE";
            // 
            // BtnBouns2
            // 
            this.BtnBouns2.Font = new System.Drawing.Font("SimSun", 15F);
            this.BtnBouns2.Location = new System.Drawing.Point(1061, 385);
            this.BtnBouns2.Margin = new System.Windows.Forms.Padding(4);
            this.BtnBouns2.Name = "BtnBouns2";
            this.BtnBouns2.Size = new System.Drawing.Size(255, 35);
            this.BtnBouns2.TabIndex = 48;
            this.BtnBouns2.Text = "Team2 Add bonus Points";
            this.BtnBouns2.UseVisualStyleBackColor = true;
            this.BtnBouns2.Click += new System.EventHandler(this.BtnBouns2_Click);
            // 
            // BtnGoal2
            // 
            this.BtnGoal2.Font = new System.Drawing.Font("SimSun", 15F);
            this.BtnGoal2.Location = new System.Drawing.Point(1061, 474);
            this.BtnGoal2.Margin = new System.Windows.Forms.Padding(4);
            this.BtnGoal2.Name = "BtnGoal2";
            this.BtnGoal2.Size = new System.Drawing.Size(255, 42);
            this.BtnGoal2.TabIndex = 47;
            this.BtnGoal2.Text = "TeamB goal score";
            this.BtnGoal2.UseVisualStyleBackColor = true;
            this.BtnGoal2.Click += new System.EventHandler(this.BtnGoal2_Click);
            // 
            // lblScore2
            // 
            this.lblScore2.AutoSize = true;
            this.lblScore2.BackColor = System.Drawing.Color.Aquamarine;
            this.lblScore2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScore2.Location = new System.Drawing.Point(1202, 142);
            this.lblScore2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblScore2.Name = "lblScore2";
            this.lblScore2.Size = new System.Drawing.Size(47, 52);
            this.lblScore2.TabIndex = 45;
            this.lblScore2.Text = "0";
            // 
            // lblScore1
            // 
            this.lblScore1.AutoSize = true;
            this.lblScore1.BackColor = System.Drawing.Color.Aquamarine;
            this.lblScore1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScore1.Location = new System.Drawing.Point(1202, 55);
            this.lblScore1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblScore1.Name = "lblScore1";
            this.lblScore1.Size = new System.Drawing.Size(47, 52);
            this.lblScore1.TabIndex = 44;
            this.lblScore1.Text = "0";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.GroupBox2.Controls.Add(this.dgvTeam2Players);
            this.GroupBox2.Font = new System.Drawing.Font("SimSun", 20F);
            this.GroupBox2.Location = new System.Drawing.Point(1349, 259);
            this.GroupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.GroupBox2.Size = new System.Drawing.Size(506, 507);
            this.GroupBox2.TabIndex = 43;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Team B Members";
            // 
            // dgvTeam2Players
            // 
            this.dgvTeam2Players.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgvTeam2Players.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTeam2Players.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTeam2Players.Location = new System.Drawing.Point(4, 43);
            this.dgvTeam2Players.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTeam2Players.Name = "dgvTeam2Players";
            this.dgvTeam2Players.RowTemplate.Height = 23;
            this.dgvTeam2Players.Size = new System.Drawing.Size(498, 460);
            this.dgvTeam2Players.TabIndex = 0;
            // 
            // TeamName2
            // 
            this.TeamName2.AutoSize = true;
            this.TeamName2.Font = new System.Drawing.Font("SimSun", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TeamName2.Location = new System.Drawing.Point(549, 161);
            this.TeamName2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TeamName2.Name = "TeamName2";
            this.TeamName2.Size = new System.Drawing.Size(130, 24);
            this.TeamName2.TabIndex = 41;
            this.TeamName2.Text = "Team2 Name";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.dgvTeam1Players);
            this.GroupBox1.Font = new System.Drawing.Font("SimSun", 20F);
            this.GroupBox1.Location = new System.Drawing.Point(545, 259);
            this.GroupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.GroupBox1.Size = new System.Drawing.Size(492, 508);
            this.GroupBox1.TabIndex = 40;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Team A Members";
            // 
            // dgvTeam1Players
            // 
            this.dgvTeam1Players.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvTeam1Players.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgvTeam1Players.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTeam1Players.Location = new System.Drawing.Point(9, 48);
            this.dgvTeam1Players.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTeam1Players.Name = "dgvTeam1Players";
            this.dgvTeam1Players.RowTemplate.Height = 23;
            this.dgvTeam1Players.Size = new System.Drawing.Size(475, 452);
            this.dgvTeam1Players.TabIndex = 0;
            // 
            // btnSaveScores
            // 
            this.btnSaveScores.Font = new System.Drawing.Font("SimSun", 15F);
            this.btnSaveScores.Location = new System.Drawing.Point(1037, 568);
            this.btnSaveScores.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveScores.Name = "btnSaveScores";
            this.btnSaveScores.Size = new System.Drawing.Size(308, 45);
            this.btnSaveScores.TabIndex = 39;
            this.btnSaveScores.Text = "Save Scores";
            this.btnSaveScores.UseVisualStyleBackColor = true;
            this.btnSaveScores.Click += new System.EventHandler(this.btnSaveScore_Click);
            // 
            // BtnGoal
            // 
            this.BtnGoal.Font = new System.Drawing.Font("SimSun", 15F);
            this.BtnGoal.Location = new System.Drawing.Point(1061, 428);
            this.BtnGoal.Margin = new System.Windows.Forms.Padding(4);
            this.BtnGoal.Name = "BtnGoal";
            this.BtnGoal.Size = new System.Drawing.Size(257, 39);
            this.BtnGoal.TabIndex = 38;
            this.BtnGoal.Text = "TeamA goal score ";
            this.BtnGoal.UseVisualStyleBackColor = true;
            this.BtnGoal.Click += new System.EventHandler(this.BtnGoal_Click);
            // 
            // BtnBouns
            // 
            this.BtnBouns.Font = new System.Drawing.Font("SimSun", 15F);
            this.BtnBouns.Location = new System.Drawing.Point(1063, 338);
            this.BtnBouns.Margin = new System.Windows.Forms.Padding(4);
            this.BtnBouns.Name = "BtnBouns";
            this.BtnBouns.Size = new System.Drawing.Size(255, 40);
            this.BtnBouns.TabIndex = 37;
            this.BtnBouns.Text = "Team1 Add bonus Points";
            this.BtnBouns.UseVisualStyleBackColor = true;
            this.BtnBouns.Click += new System.EventHandler(this.BtnBouns_Click);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(24, 82);
            this.Label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(0, 15);
            this.Label4.TabIndex = 35;
            // 
            // lblMatchNumber
            // 
            this.lblMatchNumber.AutoSize = true;
            this.lblMatchNumber.Font = new System.Drawing.Font("SimSun", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblMatchNumber.Location = new System.Drawing.Point(1365, 75);
            this.lblMatchNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMatchNumber.Name = "lblMatchNumber";
            this.lblMatchNumber.Size = new System.Drawing.Size(154, 24);
            this.lblMatchNumber.TabIndex = 33;
            this.lblMatchNumber.Text = "Match Number";
            // 
            // txtTeam2
            // 
            this.txtTeam2.Font = new System.Drawing.Font("SimSun", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTeam2.Location = new System.Drawing.Point(702, 156);
            this.txtTeam2.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeam2.Name = "txtTeam2";
            this.txtTeam2.ReadOnly = true;
            this.txtTeam2.Size = new System.Drawing.Size(236, 36);
            this.txtTeam2.TabIndex = 42;
            // 
            // TeamName1
            // 
            this.TeamName1.AutoSize = true;
            this.TeamName1.Font = new System.Drawing.Font("SimSun", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TeamName1.Location = new System.Drawing.Point(549, 74);
            this.TeamName1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TeamName1.Name = "TeamName1";
            this.TeamName1.Size = new System.Drawing.Size(130, 24);
            this.TeamName1.TabIndex = 32;
            this.TeamName1.Text = "Team1 Name";
            // 
            // txtTeam1
            // 
            this.txtTeam1.Font = new System.Drawing.Font("SimSun", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTeam1.Location = new System.Drawing.Point(702, 69);
            this.txtTeam1.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeam1.Name = "txtTeam1";
            this.txtTeam1.ReadOnly = true;
            this.txtTeam1.Size = new System.Drawing.Size(237, 36);
            this.txtTeam1.TabIndex = 31;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgvMatches);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(17, 16);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(520, 754);
            this.groupBox3.TabIndex = 57;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Matches(Double click to start the match)";
            // 
            // dgvMatches
            // 
            this.dgvMatches.AllowUserToAddRows = false;
            this.dgvMatches.AllowUserToDeleteRows = false;
            this.dgvMatches.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvMatches.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgvMatches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMatches.Location = new System.Drawing.Point(4, 31);
            this.dgvMatches.Margin = new System.Windows.Forms.Padding(4);
            this.dgvMatches.Name = "dgvMatches";
            this.dgvMatches.ReadOnly = true;
            this.dgvMatches.RowTemplate.Height = 23;
            this.dgvMatches.Size = new System.Drawing.Size(508, 719);
            this.dgvMatches.TabIndex = 0;
            this.dgvMatches.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMatches_CellMouseDoubleClick);
            this.dgvMatches.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvMatches_RowPrePaint);
            // 
            // lblMatchNum
            // 
            this.lblMatchNum.AutoSize = true;
            this.lblMatchNum.Font = new System.Drawing.Font("Microsoft YaHei", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblMatchNum.Location = new System.Drawing.Point(1587, 62);
            this.lblMatchNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMatchNum.Name = "lblMatchNum";
            this.lblMatchNum.Size = new System.Drawing.Size(0, 39);
            this.lblMatchNum.TabIndex = 58;
            // 
            // btnSemiFinalMatch
            // 
            this.btnSemiFinalMatch.Font = new System.Drawing.Font("SimSun", 15F);
            this.btnSemiFinalMatch.Location = new System.Drawing.Point(1037, 621);
            this.btnSemiFinalMatch.Margin = new System.Windows.Forms.Padding(4);
            this.btnSemiFinalMatch.Name = "btnSemiFinalMatch";
            this.btnSemiFinalMatch.Size = new System.Drawing.Size(308, 70);
            this.btnSemiFinalMatch.TabIndex = 59;
            this.btnSemiFinalMatch.Text = "     Semifinals      (after qual round)";
            this.btnSemiFinalMatch.UseVisualStyleBackColor = true;
            this.btnSemiFinalMatch.Click += new System.EventHandler(this.btnSemiFinal_Click);
            // 
            // btnFinals
            // 
            this.btnFinals.Font = new System.Drawing.Font("SimSun", 15F);
            this.btnFinals.Location = new System.Drawing.Point(1037, 699);
            this.btnFinals.Margin = new System.Windows.Forms.Padding(4);
            this.btnFinals.Name = "btnFinals";
            this.btnFinals.Size = new System.Drawing.Size(308, 60);
            this.btnFinals.TabIndex = 60;
            this.btnFinals.Text = "    Finals       (after semifinal)";
            this.btnFinals.UseVisualStyleBackColor = true;
            this.btnFinals.Click += new System.EventHandler(this.btnFinals_Click);
            // 
            // lblRoundNum
            // 
            this.lblRoundNum.AutoSize = true;
            this.lblRoundNum.Font = new System.Drawing.Font("SimSun", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblRoundNum.Location = new System.Drawing.Point(1367, 154);
            this.lblRoundNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRoundNum.Name = "lblRoundNum";
            this.lblRoundNum.Size = new System.Drawing.Size(154, 24);
            this.lblRoundNum.TabIndex = 61;
            this.lblRoundNum.Text = "Round Number";
            // 
            // cbRoundNum
            // 
            this.cbRoundNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cbRoundNum.Font = new System.Drawing.Font("SimSun", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbRoundNum.FormattingEnabled = true;
            this.cbRoundNum.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbRoundNum.Location = new System.Drawing.Point(1547, 150);
            this.cbRoundNum.Margin = new System.Windows.Forms.Padding(4);
            this.cbRoundNum.Name = "cbRoundNum";
            this.cbRoundNum.Size = new System.Drawing.Size(160, 33);
            this.cbRoundNum.TabIndex = 62;
            // 
            // btnChangeRound
            // 
            this.btnChangeRound.Font = new System.Drawing.Font("SimSun", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnChangeRound.Location = new System.Drawing.Point(1063, 290);
            this.btnChangeRound.Margin = new System.Windows.Forms.Padding(4);
            this.btnChangeRound.Name = "btnChangeRound";
            this.btnChangeRound.Size = new System.Drawing.Size(255, 36);
            this.btnChangeRound.TabIndex = 63;
            this.btnChangeRound.Text = "Next Round";
            this.btnChangeRound.UseVisualStyleBackColor = true;
            this.btnChangeRound.Click += new System.EventHandler(this.btnChangeRound_Click);
            // 
            // frmScoring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1868, 953);
            this.Controls.Add(this.btnChangeRound);
            this.Controls.Add(this.cbRoundNum);
            this.Controls.Add(this.lblRoundNum);
            this.Controls.Add(this.btnFinals);
            this.Controls.Add(this.btnSemiFinalMatch);
            this.Controls.Add(this.lblMatchNum);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.lblBounsPoint2);
            this.Controls.Add(this.lblBounsPoint1);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Bouns);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.BtnIntercept);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.BtnBouns2);
            this.Controls.Add(this.BtnGoal2);
            this.Controls.Add(this.lblScore2);
            this.Controls.Add(this.lblScore1);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.TeamName2);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.btnSaveScores);
            this.Controls.Add(this.BtnGoal);
            this.Controls.Add(this.BtnBouns);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.lblMatchNumber);
            this.Controls.Add(this.txtTeam2);
            this.Controls.Add(this.TeamName1);
            this.Controls.Add(this.txtTeam1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmScoring";
            this.Text = "Score";
            this.GroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeam2Players)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeam1Players)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMatches)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label lblBounsPoint2;
        internal System.Windows.Forms.Label lblBounsPoint1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Bouns;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Button BtnIntercept;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button BtnBouns2;
        internal System.Windows.Forms.Button BtnGoal2;
        internal System.Windows.Forms.Label lblScore2;
        internal System.Windows.Forms.Label lblScore1;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Label TeamName2;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Button btnSaveScores;
        internal System.Windows.Forms.Button BtnGoal;
        internal System.Windows.Forms.Button BtnBouns;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label lblMatchNumber;
        internal System.Windows.Forms.TextBox txtTeam2;
        internal System.Windows.Forms.Label TeamName1;
        internal System.Windows.Forms.TextBox txtTeam1;
        private GroupBox groupBox3;
        private DataGridView dgvMatches;
        private DataGridView dgvTeam2Players;
        private DataGridView dgvTeam1Players;
        private Label lblMatchNum;
        private Button btnSemiFinalMatch;
        private Button btnFinals;
        private Label lblRoundNum;
        private ComboBox cbRoundNum;
        private Button btnChangeRound;
    }
}

