﻿namespace Tournaments
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBaseInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.socresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmScore = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRoundReports = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRankReport = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.socresToolStripMenuItem,
            this.reportsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1012, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmBaseInfo});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(66, 21);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // tsmBaseInfo
            // 
            this.tsmBaseInfo.Name = "tsmBaseInfo";
            this.tsmBaseInfo.Size = new System.Drawing.Size(127, 22);
            this.tsmBaseInfo.Text = "BaseInfo";
            this.tsmBaseInfo.Click += new System.EventHandler(this.tsmBaseInfo_Click);
            // 
            // socresToolStripMenuItem
            // 
            this.socresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmScore});
            this.socresToolStripMenuItem.Name = "socresToolStripMenuItem";
            this.socresToolStripMenuItem.Size = new System.Drawing.Size(59, 21);
            this.socresToolStripMenuItem.Text = "Scores";
            // 
            // tsmScore
            // 
            this.tsmScore.Name = "tsmScore";
            this.tsmScore.Size = new System.Drawing.Size(109, 22);
            this.tsmScore.Text = "Score";
            this.tsmScore.Click += new System.EventHandler(this.tsmScore_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmRoundReports,
            this.tsmRankReport});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(66, 21);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // tsmRoundReports
            // 
            this.tsmRoundReports.Name = "tsmRoundReports";
            this.tsmRoundReports.Size = new System.Drawing.Size(158, 22);
            this.tsmRoundReports.Text = "Round Report";
            this.tsmRoundReports.Click += new System.EventHandler(this.tsmRoundReports_Click);
            // 
            // tsmRankReport
            // 
            this.tsmRankReport.Name = "tsmRankReport";
            this.tsmRankReport.Size = new System.Drawing.Size(158, 22);
            this.tsmRankReport.Text = "Rank Report";
            this.tsmRankReport.Click += new System.EventHandler(this.tsmRankReport_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 490);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frmMain";
            this.Text = "Tournament Software";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem socresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmBaseInfo;
        private System.Windows.Forms.ToolStripMenuItem tsmScore;
        private System.Windows.Forms.ToolStripMenuItem tsmRoundReports;
        private System.Windows.Forms.ToolStripMenuItem tsmRankReport;
    }
}