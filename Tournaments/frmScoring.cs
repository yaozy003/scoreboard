﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using Microsoft.VisualBasic;
using Tournaments.Api;
using Tournaments.Services;
using Tournaments.Models;
using Tournaments.Models.InfoModels;
using Tournaments.Extend;

namespace Tournaments
{
    public partial class frmScoring : Form
    {
        public frmScoring()
        {
            InitializeComponent();
            Scoring_Load();
        }

        #region Private fields
        private int bonus1 = 0;
        private int bonus2 = 0;
        private int score1 = 0;
        private int score2 = 0;
        // back file name
        private const string fileName = "score.bak";
        private const string tournamentFile = "registration.bak";
        private int cbRoundsIndex = 0;
        private string matchNum = string.Empty;
        private MatchTypeEnum matchType = MatchTypeEnum.qual;
        private ScoreMatchTypeEnum scoreType = ScoreMatchTypeEnum.qual;
        private RoundModel currentRound = new RoundModel();
        RegistrationModel registration = new RegistrationModel();
        ScoreModel score = new ScoreModel();
        #endregion
        ApiService apiService = new ApiService();
        CommonService commonService = new CommonService();

        IList<RoundModel> qualRounds = new List<RoundModel>(); // qual rounds
        IList<RoundModel> finalRounds = new List<RoundModel>(); // final rounds

        IList<string> qualIsOver = new List<string>(); // qual rounds is over
        IList<string> finalIsOver = new List<string>(); // final rounds is over


        private void Scoring_Load()
        {
            try
            {
                // if r equals null display message box.
                RegistrationModel r = commonService.GetRegistration();
                if (r == null || string.IsNullOrEmpty(r.TouranmentId))
                {
                    MessageBox.Show("Can't read tournament.");
                    return;
                }
                registration = r;

                //Determine if the qualify rounds or final rounds are over
                qualIsOver = commonService.GetQualIsOver(registration.TouranmentId);
                finalIsOver = commonService.GetFinalIsOver(registration.TouranmentId);

                ScoreModel s = commonService.GetScore();

                // if s equals null, set it to a new game, re-generate game result.
                if (s == null || string.IsNullOrEmpty(s.Touranment.Id))
                {
                    qualRounds = commonService.GenerateQualMatches(r.TouranmentId);
                    cbRoundNum.SelectedIndex = cbRoundsIndex;
                    InitialMatches(qualRounds);
                    SaveScore();
                }
                else if (s != null && !string.IsNullOrEmpty(s.Touranment.Id))
                {
                    //registration tournament
                    if (s.Touranment.Id.Equals(registration.TouranmentId))
                    {
                        score = s;
                        RecoverMatches(score);
                    }
                    else
                    {
                        DialogResult result = MessageBox.Show("Begin a new touranment?", "Tip", MessageBoxButtons.OKCancel);
                        if (result == DialogResult.OK)
                        {
                            qualRounds = commonService.GenerateQualMatches(r.TouranmentId);
                            cbRoundNum.SelectedIndex = cbRoundsIndex;
                            InitialMatches(qualRounds);
                            SaveScore();
                        }
                        else
                        {
                            score = s;
                            RecoverMatches(score);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }

        /// <summary>
        /// Save score status
        /// </summary>
        private void SaveScore()
        {
            try
            {
                score.Touranment.Id = registration.TouranmentId;
                score.Touranment.Name = registration.TouranmentName;
                score.Score1 = score1;
                score.Score2 = score2;
                score.Bonus1 = bonus1;
                score.Bonus2 = bonus2;
                score.MatchNum = matchNum;
                score.CurrentRound = cbRoundsIndex;
                score.MatchType = matchType;
                score.ScoreType = scoreType;
                score.CurrentMatch = currentRound;
                FileHelper.SaveContent(score.ConvertToString(), fileName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Binding matches to dgvMatches
        /// </summary>
        /// <param name="roundsList"></param>
        private void InitialMatches(IList<RoundModel> roundsList)
        {
            try
            {
                dgvMatches.DataSource = null;
                // Create data table.
                DataTable data = new DataTable();
                DataColumn roundNum = new DataColumn("Round Num", typeof(string));
                data.Columns.Add(roundNum);
                DataColumn team1 = new DataColumn("Team 1", typeof(string));
                data.Columns.Add(team1);
                DataColumn team2 = new DataColumn("Team 2", typeof(string));
                data.Columns.Add(team2);
                foreach (RoundModel model in roundsList)
                {
                    DataRow row = data.NewRow();
                    row["Round Num"] = model.Round;
                    if (model.Teams[0] == null)
                    {
                        row["Team 1"] = string.Empty;
                    }
                    else
                    {
                        row["Team 1"] = model.Teams[0].Name;
                    }

                    if (model.Teams[1] == null)
                    {
                        row["Team 2"] = string.Empty;
                    }
                    else
                    {
                        row["Team 2"] = model.Teams[1].Name;
                    }

                    data.Rows.Add(row.ItemArray);
                }
                dgvMatches.DataSource = data;
                dgvMatches.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RecoverMatches(ScoreModel model)
        {
            try
            {
                score1 = model.Score1;
                score2 = model.Score2;
                bonus1 = model.Bonus1;
                bonus2 = model.Bonus2;

                lblScore1.Text = score1.ToString();
                lblScore2.Text = score2.ToString();
                lblBounsPoint1.Text = bonus1.ToString();
                lblBounsPoint2.Text = bonus2.ToString();

                cbRoundsIndex = model.CurrentRound;
                cbRoundNum.SelectedIndex = cbRoundsIndex;

                currentRound = score.CurrentMatch;
                //dispaly team info.
                if (currentRound != null && currentRound.Teams.Count == 2)
                {
                    txtTeam1.Text = currentRound.Teams[0].Name;
                    txtTeam1.Tag = currentRound.Teams[0].Id;
                    txtTeam2.Text = currentRound.Teams[1].Name;
                    txtTeam2.Tag = currentRound.Teams[1].Id;
                    DisplayPlayers(currentRound.Teams[0].Id, dgvTeam1Players);
                    DisplayPlayers(currentRound.Teams[1].Id, dgvTeam2Players);

                }
                matchNum = score.MatchNum;
                lblMatchNum.Text = matchNum;
                qualRounds = commonService.GenerateQualMatches(model.Touranment.Id);

                qualIsOver = commonService.GetQualIsOver(model.Touranment.Id);
                finalIsOver = commonService.GetFinalIsOver(model.Touranment.Id);
                // check type of the match
                if (model.ScoreType == ScoreMatchTypeEnum.qual)
                {
                    InitialMatches(qualRounds);
                }
                else if (model.ScoreType == ScoreMatchTypeEnum.semiFinal)
                {
                    finalRounds = commonService.GenerateSemiFinals(model.Touranment.Id);
                    InitialMatches(finalRounds);
                }
                else
                {
                    finalRounds = commonService.GenerateFinals(model.Touranment.Id);
                    InitialMatches(finalRounds);
                }
                matchType = score.MatchType;
                scoreType = score.ScoreType;
            }
            catch (Exception)
            {
                throw;
            }
        }



        private void BtnBouns_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTeam1.Text) || string.IsNullOrEmpty(txtTeam2.Text))
            {
                MessageBox.Show("Please selected one match !");
                return;
            }
            // if bonus less than 3, bound add 1 then return new bonus point'
            if (bonus1 < 3)
            {
                bonus1 += 1;
            }
            lblBounsPoint1.Text = bonus1.ToString();
            SaveScore();
        }

        private void BtnBouns2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTeam1.Text) || string.IsNullOrEmpty(txtTeam2.Text))
            {
                MessageBox.Show("Please selected one match !");
                return;
            }
            // if bouns less than 3, bound add 1 then return new bonus point'
            if (bonus2 < 3)
            {
                bonus2 += 1;
            }
            lblBounsPoint2.Text = bonus2.ToString();
            SaveScore();
        }



        /// <summary>
        /// Save scores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveScore_Click(object sender, EventArgs e)
        {

            try
            {
                // game match cannot be null.
                if (string.IsNullOrEmpty(txtTeam1.Text) || string.IsNullOrEmpty(txtTeam2.Text))
                {
                    MessageBox.Show("Please selected one match !");
                    return;
                }
                // add bonus to total score.
                if (bonus1 != 0 || bonus2 != 0)
                {
                    MessageBox.Show("Please calculate the socres !");
                    return;
                }

                DataRow selected = (dgvMatches.DataSource as DataTable).Rows[rowIndex];
                string roundNum = selected["Round Num"].ToString();

                if (matchType == MatchTypeEnum.qual)
                {
                    if (qualIsOver.Contains(roundNum))
                    {
                        MessageBox.Show("The match is over.");
                        return;
                    }
                    RoundModel model = qualRounds.SingleOrDefault(x => x.Round.Equals(roundNum));
                    if (model == null)
                    {
                        MessageBox.Show("Can not find the selected round !");
                        return;
                    }
                    //if fair to update match to api.
                    var tmp = apiService.UpdateMatch(score.Touranment.Id, txtTeam1.Tag.ToString(), MatchTypeEnum.qual, int.Parse(roundNum), score1);
                    if (!tmp)
                    {
                        MessageBox.Show("Update scores false");
                        return;
                    }
                    tmp = apiService.UpdateMatch(score.Touranment.Id, txtTeam2.Tag.ToString(), MatchTypeEnum.qual, int.Parse(roundNum), score2);
                    if (!tmp)
                    {
                        MessageBox.Show("Update scores false");
                        return;
                    }

                    model.Teams[0].Score = score1.ToString();
                    model.Teams[1].Score = score2.ToString();
                    dgvMatches.Rows[rowIndex].DefaultCellStyle.BackColor = Color.Black;
                }
                else
                {
                    if (finalIsOver.Contains(roundNum))
                    {
                        MessageBox.Show("The match is over.");
                        return;
                    }
                    RoundModel model = finalRounds.SingleOrDefault(x => x.Round.Equals(roundNum));
                    if (model == null)
                    {
                        MessageBox.Show("Can not find the selected round !");
                        return;
                    }

                    var tmp = apiService.UpdateMatch(score.Touranment.Id, txtTeam1.Tag.ToString(), MatchTypeEnum.final, int.Parse(matchNum), int.Parse(lblScore1.Text));
                    if (!tmp)
                    {
                        MessageBox.Show("Update scores false");
                        return;
                    }
                    tmp = apiService.UpdateMatch(score.Touranment.Id, txtTeam2.Tag.ToString(), MatchTypeEnum.final, int.Parse(matchNum), int.Parse(lblScore2.Text));
                    if (!tmp)
                    {
                        MessageBox.Show("Update scores false");
                        return;
                    }
                    model.Teams[0].Score = score1.ToString();
                    model.Teams[1].Score = score2.ToString();
                    dgvMatches.Rows[rowIndex].DefaultCellStyle.BackColor = Color.Black;
                }
                // reset all text' 
                txtTeam1.Text = "";
                txtTeam1.Tag = "";
                txtTeam2.Text = "";
                txtTeam2.Tag = "";
                lblBounsPoint1.Text = "0";
                lblBounsPoint2.Text = "0";
                lblScore1.Text = "0";
                lblScore2.Text = " 0";
                cbRoundsIndex = 0;
                cbRoundNum.SelectedIndex = cbRoundsIndex;
                bonus1 = 0;
                bonus2 = 0;
                score1 = 0;
                score2 = 0;
                currentRound.RoundEnd = true;

                qualIsOver = commonService.GetQualIsOver(score.Touranment.Id);
                finalIsOver = commonService.GetFinalIsOver(score.Touranment.Id);
                SaveScore();
                MessageBox.Show("Save successfully !");

            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// BtnGoal click events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGoal_Click(object sender, EventArgs e)
        {
            // team #1 scores for 5 points + bonus
            try
            {
                if (string.IsNullOrEmpty(txtTeam1.Text) || string.IsNullOrEmpty(txtTeam2.Text))
                {
                    MessageBox.Show("Please selected one match !");
                    return;
                }
                score1 = score1 + 5 + bonus1;
                lblScore1.Text = score1.ToString();
                bonus1 = 0;
                lblBounsPoint1.Text = bonus1.ToString();

                SaveScore();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }

        /// <summary>
        /// BtnGoal2 click events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGoal2_Click(object sender, EventArgs e)
        {
            // team #2 scores for 5 points + bonus
            try
            {
                if (string.IsNullOrEmpty(txtTeam1.Text) || string.IsNullOrEmpty(txtTeam2.Text))
                {
                    MessageBox.Show("Please selected one match !");
                    return;
                }
                score2 = score2 + 5 + bonus2;
                lblScore2.Text = score2.ToString();
                bonus2 = 0;
                lblBounsPoint2.Text = bonus2.ToString();
                SaveScore();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }

        /// <summary>
        /// Intercept Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnIntercept_Click(object sender, EventArgs e)
        {
            // intercept the ball, bonus point =0' 
            lblBounsPoint1.Text = "0";
            bonus1 = 0;
            lblBounsPoint2.Text = "0";
            bonus2 = 0;
        }

        int rowIndex = 0;

        /// <summary>
        /// double click on a match to finish it and start a new one
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvMatches_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                //Ask the user to confirm
                DialogResult dialog = MessageBox.Show("Start next match?", "Tips", MessageBoxButtons.OKCancel);
                if (dialog == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    bonus1 = 0;
                    bonus2 = 0;
                    score1 = 0;
                    score2 = 0;

                    var r = (sender as DataGridView).CurrentRow;
                    matchNum = r.Cells["Round Num"].Value.ToString();

                    //Can not repeat a previous match
                    if (matchType == MatchTypeEnum.qual)
                    {
                        bool exists = qualIsOver.Any(x => x.Equals(matchNum));
                        if (exists)
                        {
                            MessageBox.Show("This match is over !");
                            return;
                        }

                    }
                    //semiFinals and finals are the same
                    else
                    {
                        bool exists = finalIsOver.Any(x => x.Equals(matchNum));
                        if (exists)
                        {
                            MessageBox.Show("This match is over !");
                            return;
                        }

                    }

                    RoundModel round;

                    //If the match is a qualify round
                    if (matchType == MatchTypeEnum.qual)
                    {
                        round = qualRounds.SingleOrDefault(x => x.Round.Equals(matchNum));
                    }
                    else
                    {
                        round = finalRounds.SingleOrDefault(x => x.Round.Equals(matchNum));
                    }

                    //Initial the match contents
                    InitialMatch(round);
                    DisplayPlayers(round.Teams[0].Id, dgvTeam1Players);
                    DisplayPlayers(round.Teams[1].Id, dgvTeam2Players);
                    lblMatchNum.Text = matchNum;
                    score.CurrentMatch = round;
                    SaveScore();
                }

                rowIndex = ((DataGridView)sender).CurrentRow.Index;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Initial the round contents
        /// </summary>
        /// <param name="round"></param>
        private void InitialMatch(RoundModel round)
        {
            try
            {
                //Load the info of new matching teams
                txtTeam1.Text = round.Teams[0].Name;
                txtTeam1.Tag = round.Teams[0].Id;
                txtTeam2.Text = round.Teams[1].Name;
                txtTeam2.Tag = round.Teams[1].Id;
                lblMatchNum.Text = round.Round;

                lblScore1.Text = "0";
                lblScore2.Text = "0";
                lblBounsPoint1.Text = "0";
                lblBounsPoint2.Text = "0";

                //Initial the scores and bonus
                score1 = 0;
                score2 = 0;
                bonus1 = 0;
                bonus2 = 0;
                cbRoundsIndex = 0;
                cbRoundNum.SelectedIndex = cbRoundsIndex;
                matchNum = lblMatchNum.Text;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Display the plays` info on the window
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="view"></param>
        private void DisplayPlayers(string teamId, DataGridView view)
        {
            //Load the data from the db and add them to the initial datatable
            try
            {
                view.DataSource = null;
                PlayersModel players = apiService.QueryPlayers(teamId);
                if (players == null)
                {
                    return;
                }
                DataTable datas = new DataTable();
                DataColumn playerId = new DataColumn("Player Id", typeof(string));
                DataColumn playerName = new DataColumn("Player Name", typeof(string));
                DataColumn yellowCard = new DataColumn("Yellow Card", typeof(int));
                DataColumn redCard = new DataColumn("Red Card", typeof(int));

                datas.Columns.Add(playerId);
                datas.Columns.Add(playerName);
                datas.Columns.Add(yellowCard);
                datas.Columns.Add(redCard);

                //Load each player in the team
                foreach (var player in players.Players)
                {
                    DataRow row = datas.NewRow();
                    row["Player Id"] = player.Id;
                    row["Player Name"] = player.Name;
                    row["Yellow Card"] = player.YellowCard;
                    row["Red Card"] = player.RedCard;

                    datas.Rows.Add(row.ItemArray);
                }

                view.DataSource = datas;
                view.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                view.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Start the semi-final match
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSemiFinal_Click(object sender, EventArgs e)
        {
            try
            {
                //check the match type first
                if (matchType == MatchTypeEnum.final)
                {
                    MessageBox.Show("Failed to generate the game");
                    return;
                }

                //See if there`s any ongoing semi-final games
                IList<RoundModel> semiRounds = new List<RoundModel>();
                semiRounds = commonService.GenerateSemiFinals(registration.TouranmentId);
                if (semiRounds == null || semiRounds.Count == 0)
                {
                    MessageBox.Show("Failed to generate the game");
                    return;
                }

                //initial the contents
                finalRounds = semiRounds;
                InitialMatches(finalRounds);
                txtTeam1.Text = string.Empty;
                txtTeam1.Tag = string.Empty;
                txtTeam2.Text = string.Empty;
                txtTeam2.Tag = string.Empty;
                lblMatchNum.Text = string.Empty;

                lblScore1.Text = "0";
                lblScore2.Text = "0";
                lblBounsPoint1.Text = "0";
                lblBounsPoint2.Text = "0";

                score1 = 0;
                score2 = 0;
                bonus1 = 0;
                bonus2 = 0;
                cbRoundsIndex = 0;
                cbRoundNum.SelectedIndex = cbRoundsIndex;
                matchNum = lblMatchNum.Text;
                dgvTeam1Players.DataSource = null;
                dgvTeam2Players.DataSource = null;
                matchType = MatchTypeEnum.final;
                scoreType = ScoreMatchTypeEnum.semiFinal;
                SaveScore();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Finals control button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFinals_Click(object sender, EventArgs e)
        {
            try
            {
                //Check if the semi-finals have finished
                IList<RoundModel> semiRounds = new List<RoundModel>();
                semiRounds = commonService.GenerateSemiFinals(registration.TouranmentId);
                if (semiRounds == null || semiRounds.Count != 2)
                {
                    MessageBox.Show("Failed to generate finals");
                    return;
                }

                IList<RoundModel> tmp = commonService.GenerateFinals(registration.TouranmentId);
                if (tmp == null || tmp.Count == 0)
                {
                    MessageBox.Show("Failed to generate finals");
                    return;
                }
                //initial finals` contents
                finalRounds = tmp;
                InitialMatches(finalRounds);

                scoreType = ScoreMatchTypeEnum.final;
                matchType = MatchTypeEnum.final;

                bonus1 = 0;
                bonus2 = 0;
                score1 = 0;
                score2 = 0;
                matchNum = string.Empty;
                cbRoundsIndex = 0;

                lblBounsPoint1.Text = bonus1.ToString();
                lblBounsPoint2.Text = bonus2.ToString();
                lblScore1.Text = score1.ToString();
                lblScore2.Text = score2.ToString();
                cbRoundNum.SelectedIndex = cbRoundsIndex;

                txtTeam1.Text = string.Empty;
                txtTeam1.Tag = string.Empty;
                txtTeam2.Text = string.Empty;
                txtTeam2.Tag = string.Empty;
                lblMatchNum.Text = string.Empty;
                dgvTeam1Players.DataSource = null;
                dgvTeam2Players.DataSource = null;
                SaveScore();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Change the current round to the next
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangeRound_Click(object sender, EventArgs e)
        {
            try
            {
                //check if already finished 3 rounds
                cbRoundsIndex++;
                if (cbRoundsIndex == 3)
                {
                    cbRoundsIndex = 0;
                    cbRoundNum.SelectedIndex = cbRoundsIndex;
                }
                else
                {
                    cbRoundNum.SelectedIndex = cbRoundsIndex;
                }

                bonus1 = 0;
                bonus2 = 0;
                score.Bonus1 = bonus1;
                score.Bonus2 = bonus2;

                SaveScore();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// If the match is over, change the backcolor to black.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvMatches_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            ChangeColor();
        }

        /// <summary>
        /// If the match is over, change the background color to indicate.
        /// </summary>
        private void ChangeColor()
        {
            try
            {
                DataTable datas = dgvMatches.DataSource as DataTable;
                for (int i = 0; i < datas.Rows.Count; i++)
                {
                    if (matchType == MatchTypeEnum.qual)
                    {
                        bool exists = qualIsOver.Contains(datas.Rows[i][0].ToString());
                        if (exists)
                        {
                            dgvMatches.Rows[i].DefaultCellStyle.BackColor = Color.Black;
                        }
                    }
                    else
                    {
                        bool exists = finalIsOver.Contains(datas.Rows[i][0].ToString());
                        if (exists)
                        {
                            dgvMatches.Rows[i].DefaultCellStyle.BackColor = Color.Black;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }


}
