﻿namespace Tournaments
{
    partial class frmRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelTournament = new System.Windows.Forms.Label();
            this.btnSetTournament = new System.Windows.Forms.Button();
            this.txtTournament = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rtbTeams = new System.Windows.Forms.RichTextBox();
            this.btnAddTeams = new System.Windows.Forms.Button();
            this.txtTeamName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rtbPlayers = new System.Windows.Forms.RichTextBox();
            this.btnAddPlayer = new System.Windows.Forms.Button();
            this.txtPlayerName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbTeams = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelTournament);
            this.groupBox1.Controls.Add(this.btnSetTournament);
            this.groupBox1.Controls.Add(this.txtTournament);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(794, 96);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setting Tournament";
            // 
            // labelTournament
            // 
            this.labelTournament.AutoSize = true;
            this.labelTournament.Location = new System.Drawing.Point(472, 42);
            this.labelTournament.Name = "labelTournament";
            this.labelTournament.Size = new System.Drawing.Size(0, 21);
            this.labelTournament.TabIndex = 4;
            // 
            // btnSetTournament
            // 
            this.btnSetTournament.Location = new System.Drawing.Point(340, 33);
            this.btnSetTournament.Name = "btnSetTournament";
            this.btnSetTournament.Size = new System.Drawing.Size(106, 38);
            this.btnSetTournament.TabIndex = 2;
            this.btnSetTournament.Text = "Register";
            this.btnSetTournament.UseVisualStyleBackColor = true;
            this.btnSetTournament.Click += new System.EventHandler(this.btnSetTournament_Click);
            // 
            // txtTournament
            // 
            this.txtTournament.Location = new System.Drawing.Point(171, 39);
            this.txtTournament.Name = "txtTournament";
            this.txtTournament.Size = new System.Drawing.Size(162, 29);
            this.txtTournament.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tournament Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rtbTeams);
            this.groupBox2.Controls.Add(this.btnAddTeams);
            this.groupBox2.Controls.Add(this.txtTeamName);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(13, 115);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 390);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Adding Teams";
            // 
            // rtbTeams
            // 
            this.rtbTeams.Location = new System.Drawing.Point(10, 77);
            this.rtbTeams.Name = "rtbTeams";
            this.rtbTeams.Size = new System.Drawing.Size(371, 288);
            this.rtbTeams.TabIndex = 3;
            this.rtbTeams.Text = "";
            // 
            // btnAddTeams
            // 
            this.btnAddTeams.Location = new System.Drawing.Point(300, 36);
            this.btnAddTeams.Name = "btnAddTeams";
            this.btnAddTeams.Size = new System.Drawing.Size(75, 28);
            this.btnAddTeams.TabIndex = 2;
            this.btnAddTeams.Text = "Add";
            this.btnAddTeams.UseVisualStyleBackColor = true;
            this.btnAddTeams.Click += new System.EventHandler(this.btnAddTeams_Click);
            // 
            // txtTeamName
            // 
            this.txtTeamName.Location = new System.Drawing.Point(68, 35);
            this.txtTeamName.Name = "txtTeamName";
            this.txtTeamName.Size = new System.Drawing.Size(226, 29);
            this.txtTeamName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rtbPlayers);
            this.groupBox3.Controls.Add(this.btnAddPlayer);
            this.groupBox3.Controls.Add(this.txtPlayerName);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.cbTeams);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(401, 115);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(406, 390);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Adding Players";
            // 
            // rtbPlayers
            // 
            this.rtbPlayers.Location = new System.Drawing.Point(7, 114);
            this.rtbPlayers.Name = "rtbPlayers";
            this.rtbPlayers.Size = new System.Drawing.Size(393, 239);
            this.rtbPlayers.TabIndex = 5;
            this.rtbPlayers.Text = "";
            // 
            // btnAddPlayer
            // 
            this.btnAddPlayer.Location = new System.Drawing.Point(316, 65);
            this.btnAddPlayer.Name = "btnAddPlayer";
            this.btnAddPlayer.Size = new System.Drawing.Size(84, 31);
            this.btnAddPlayer.TabIndex = 4;
            this.btnAddPlayer.Text = "Add";
            this.btnAddPlayer.UseVisualStyleBackColor = true;
            this.btnAddPlayer.Click += new System.EventHandler(this.btnAddPlayer_Click);
            // 
            // txtPlayerName
            // 
            this.txtPlayerName.Location = new System.Drawing.Point(72, 67);
            this.txtPlayerName.Name = "txtPlayerName";
            this.txtPlayerName.Size = new System.Drawing.Size(238, 29);
            this.txtPlayerName.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 21);
            this.label4.TabIndex = 2;
            this.label4.Text = "Name:";
            // 
            // cbTeams
            // 
            this.cbTeams.FormattingEnabled = true;
            this.cbTeams.Location = new System.Drawing.Point(64, 28);
            this.cbTeams.Name = "cbTeams";
            this.cbTeams.Size = new System.Drawing.Size(336, 29);
            this.cbTeams.TabIndex = 1;
            this.cbTeams.SelectedIndexChanged += new System.EventHandler(this.cbTeams_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "Team";
            // 
            // frmRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 517);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmRegistration";
            this.Text = "Tournament Information Setting";
            this.Load += new System.EventHandler(this.frmRegistration_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSetTournament;
        private System.Windows.Forms.TextBox txtTournament;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTeamName;
        private System.Windows.Forms.Button btnAddTeams;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbTeams;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAddPlayer;
        private System.Windows.Forms.TextBox txtPlayerName;
        private System.Windows.Forms.RichTextBox rtbTeams;
        private System.Windows.Forms.RichTextBox rtbPlayers;
        private System.Windows.Forms.Label labelTournament;
    }
}