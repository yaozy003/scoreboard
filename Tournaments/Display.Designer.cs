﻿namespace Tournaments
{
    partial class Display
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_round = new System.Windows.Forms.Label();
            this.lb_bonus_1 = new System.Windows.Forms.Label();
            this.DisplayBouns2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.lb_score_2 = new System.Windows.Forms.Label();
            this.lb_score_1 = new System.Windows.Forms.Label();
            this.Button1 = new System.Windows.Forms.Button();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.TeamMember2 = new System.Windows.Forms.Label();
            this.DisplayBouns = new System.Windows.Forms.Label();
            this.DisplayScore = new System.Windows.Forms.Label();
            this.ListView2 = new System.Windows.Forms.ListView();
            this.lb_bonus_2 = new System.Windows.Forms.Label();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.BtnAddToList = new System.Windows.Forms.Button();
            this.txtMembers = new System.Windows.Forms.TextBox();
            this.TeamMember1 = new System.Windows.Forms.Label();
            this.ListView1 = new System.Windows.Forms.ListView();
            this.DisPlayRoundNumber = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.GroupBox2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lb_round
            // 
            this.lb_round.AutoSize = true;
            this.lb_round.Font = new System.Drawing.Font("SimSun", 20F);
            this.lb_round.Location = new System.Drawing.Point(311, 73);
            this.lb_round.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_round.Name = "lb_round";
            this.lb_round.Size = new System.Drawing.Size(32, 34);
            this.lb_round.TabIndex = 47;
            this.lb_round.Text = "0";
            // 
            // lb_bonus_1
            // 
            this.lb_bonus_1.AutoSize = true;
            this.lb_bonus_1.Font = new System.Drawing.Font("SimSun", 20F);
            this.lb_bonus_1.Location = new System.Drawing.Point(95, 248);
            this.lb_bonus_1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_bonus_1.Name = "lb_bonus_1";
            this.lb_bonus_1.Size = new System.Drawing.Size(32, 34);
            this.lb_bonus_1.TabIndex = 45;
            this.lb_bonus_1.Text = "0";
            // 
            // DisplayBouns2
            // 
            this.DisplayBouns2.AutoSize = true;
            this.DisplayBouns2.Font = new System.Drawing.Font("SimSun", 20F);
            this.DisplayBouns2.Location = new System.Drawing.Point(997, 165);
            this.DisplayBouns2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DisplayBouns2.Name = "DisplayBouns2";
            this.DisplayBouns2.Size = new System.Drawing.Size(236, 34);
            this.DisplayBouns2.TabIndex = 44;
            this.DisplayBouns2.Text = "Bouns Point :";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.Aquamarine;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F);
            this.Label1.Location = new System.Drawing.Point(595, 165);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(90, 135);
            this.Label1.TabIndex = 43;
            this.Label1.Text = ":";
            // 
            // lb_score_2
            // 
            this.lb_score_2.AutoSize = true;
            this.lb_score_2.BackColor = System.Drawing.Color.Aquamarine;
            this.lb_score_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F);
            this.lb_score_2.Location = new System.Drawing.Point(697, 184);
            this.lb_score_2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_score_2.Name = "lb_score_2";
            this.lb_score_2.Size = new System.Drawing.Size(158, 113);
            this.lb_score_2.TabIndex = 42;
            this.lb_score_2.Text = "  0";
            // 
            // lb_score_1
            // 
            this.lb_score_1.AutoSize = true;
            this.lb_score_1.BackColor = System.Drawing.Color.Aquamarine;
            this.lb_score_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F);
            this.lb_score_1.Location = new System.Drawing.Point(451, 183);
            this.lb_score_1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_score_1.Name = "lb_score_1";
            this.lb_score_1.Size = new System.Drawing.Size(104, 113);
            this.lb_score_1.TabIndex = 41;
            this.lb_score_1.Text = "0";
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(237, 105);
            this.Button1.Margin = new System.Windows.Forms.Padding(4);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(0, 0);
            this.Button1.TabIndex = 12;
            this.Button1.Text = "Add to List";
            this.Button1.UseVisualStyleBackColor = true;
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(12, 105);
            this.TextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(0, 46);
            this.TextBox1.TabIndex = 3;
            // 
            // TeamMember2
            // 
            this.TeamMember2.AutoSize = true;
            this.TeamMember2.Location = new System.Drawing.Point(8, 42);
            this.TeamMember2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TeamMember2.Name = "TeamMember2";
            this.TeamMember2.Size = new System.Drawing.Size(219, 34);
            this.TeamMember2.TabIndex = 2;
            this.TeamMember2.Text = "Team Members";
            // 
            // DisplayBouns
            // 
            this.DisplayBouns.AutoSize = true;
            this.DisplayBouns.Font = new System.Drawing.Font("SimSun", 20F);
            this.DisplayBouns.Location = new System.Drawing.Point(8, 165);
            this.DisplayBouns.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DisplayBouns.Name = "DisplayBouns";
            this.DisplayBouns.Size = new System.Drawing.Size(236, 34);
            this.DisplayBouns.TabIndex = 40;
            this.DisplayBouns.Text = "Bouns Point :";
            // 
            // DisplayScore
            // 
            this.DisplayScore.AutoSize = true;
            this.DisplayScore.Font = new System.Drawing.Font("Rockwell", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisplayScore.Location = new System.Drawing.Point(404, 20);
            this.DisplayScore.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DisplayScore.Name = "DisplayScore";
            this.DisplayScore.Size = new System.Drawing.Size(455, 134);
            this.DisplayScore.TabIndex = 39;
            this.DisplayScore.Text = "SCORE";
            // 
            // ListView2
            // 
            this.ListView2.Location = new System.Drawing.Point(8, 80);
            this.ListView2.Margin = new System.Windows.Forms.Padding(4);
            this.ListView2.Name = "ListView2";
            this.ListView2.Size = new System.Drawing.Size(483, 244);
            this.ListView2.TabIndex = 4;
            this.ListView2.UseCompatibleStateImageBehavior = false;
            // 
            // lb_bonus_2
            // 
            this.lb_bonus_2.AutoSize = true;
            this.lb_bonus_2.Font = new System.Drawing.Font("SimSun", 20F);
            this.lb_bonus_2.Location = new System.Drawing.Point(1099, 233);
            this.lb_bonus_2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_bonus_2.Name = "lb_bonus_2";
            this.lb_bonus_2.Size = new System.Drawing.Size(32, 34);
            this.lb_bonus_2.TabIndex = 46;
            this.lb_bonus_2.Text = "0";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.Button1);
            this.GroupBox2.Controls.Add(this.TextBox1);
            this.GroupBox2.Controls.Add(this.TeamMember2);
            this.GroupBox2.Controls.Add(this.ListView2);
            this.GroupBox2.Font = new System.Drawing.Font("SimSun", 20F);
            this.GroupBox2.Location = new System.Drawing.Point(819, 378);
            this.GroupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.GroupBox2.Size = new System.Drawing.Size(492, 334);
            this.GroupBox2.TabIndex = 37;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "View Team Members";
            // 
            // BtnAddToList
            // 
            this.BtnAddToList.Location = new System.Drawing.Point(237, 105);
            this.BtnAddToList.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAddToList.Name = "BtnAddToList";
            this.BtnAddToList.Size = new System.Drawing.Size(0, 0);
            this.BtnAddToList.TabIndex = 12;
            this.BtnAddToList.Text = "Add to List";
            this.BtnAddToList.UseVisualStyleBackColor = true;
            // 
            // txtMembers
            // 
            this.txtMembers.Location = new System.Drawing.Point(12, 105);
            this.txtMembers.Margin = new System.Windows.Forms.Padding(4);
            this.txtMembers.Name = "txtMembers";
            this.txtMembers.Size = new System.Drawing.Size(0, 46);
            this.txtMembers.TabIndex = 3;
            // 
            // TeamMember1
            // 
            this.TeamMember1.AutoSize = true;
            this.TeamMember1.Location = new System.Drawing.Point(8, 42);
            this.TeamMember1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TeamMember1.Name = "TeamMember1";
            this.TeamMember1.Size = new System.Drawing.Size(219, 34);
            this.TeamMember1.TabIndex = 2;
            this.TeamMember1.Text = "Team Members";
            // 
            // ListView1
            // 
            this.ListView1.Location = new System.Drawing.Point(8, 80);
            this.ListView1.Margin = new System.Windows.Forms.Padding(4);
            this.ListView1.Name = "ListView1";
            this.ListView1.Size = new System.Drawing.Size(483, 244);
            this.ListView1.TabIndex = 4;
            this.ListView1.UseCompatibleStateImageBehavior = false;
            // 
            // DisPlayRoundNumber
            // 
            this.DisPlayRoundNumber.AutoSize = true;
            this.DisPlayRoundNumber.Font = new System.Drawing.Font("SimSun", 20F);
            this.DisPlayRoundNumber.Location = new System.Drawing.Point(8, 73);
            this.DisPlayRoundNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DisPlayRoundNumber.Name = "DisPlayRoundNumber";
            this.DisPlayRoundNumber.Size = new System.Drawing.Size(253, 34);
            this.DisPlayRoundNumber.TabIndex = 38;
            this.DisPlayRoundNumber.Text = "Round Number :";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.BtnAddToList);
            this.GroupBox1.Controls.Add(this.txtMembers);
            this.GroupBox1.Controls.Add(this.TeamMember1);
            this.GroupBox1.Controls.Add(this.ListView1);
            this.GroupBox1.Font = new System.Drawing.Font("SimSun", 20F);
            this.GroupBox1.Location = new System.Drawing.Point(41, 378);
            this.GroupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.GroupBox1.Size = new System.Drawing.Size(492, 334);
            this.GroupBox1.TabIndex = 36;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "View Team Members";
            // 
            // Display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 715);
            this.Controls.Add(this.lb_round);
            this.Controls.Add(this.lb_bonus_1);
            this.Controls.Add(this.DisplayBouns2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.lb_score_2);
            this.Controls.Add(this.lb_score_1);
            this.Controls.Add(this.DisplayBouns);
            this.Controls.Add(this.DisplayScore);
            this.Controls.Add(this.lb_bonus_2);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.DisPlayRoundNumber);
            this.Controls.Add(this.GroupBox1);
            this.Name = "Display";
            this.Text = "Display";
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label lb_round;
        internal System.Windows.Forms.Label lb_bonus_1;
        internal System.Windows.Forms.Label DisplayBouns2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label lb_score_2;
        internal System.Windows.Forms.Label lb_score_1;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.Label TeamMember2;
        internal System.Windows.Forms.Label DisplayBouns;
        internal System.Windows.Forms.Label DisplayScore;
        internal System.Windows.Forms.ListView ListView2;
        internal System.Windows.Forms.Label lb_bonus_2;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Button BtnAddToList;
        internal System.Windows.Forms.TextBox txtMembers;
        internal System.Windows.Forms.Label TeamMember1;
        internal System.Windows.Forms.ListView ListView1;
        internal System.Windows.Forms.Label DisPlayRoundNumber;
        internal System.Windows.Forms.GroupBox GroupBox1;
    }
}