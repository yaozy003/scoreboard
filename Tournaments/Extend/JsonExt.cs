﻿using Newtonsoft.Json;
namespace Tournaments.Extend
{
    public static class JsonExt
    {
        public static T ToObject<T>(this string json)
        {
            T obj = JsonConvert.DeserializeObject<T>(json);

            return obj;
        }

        public static string ConvertToString(this object obj)
        {
            string result = JsonConvert.SerializeObject(obj);

            return result;
        }
    }
}
