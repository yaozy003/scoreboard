﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tournaments.Extend;
using Tournaments.Models;
using Tournaments.Services;

namespace Tournaments
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            
        }

        
        /// <summary>
        /// Open base setting windows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmBaseInfo_Click(object sender, EventArgs e)
        {
            try
            {
                    frmRegistration frmSetting= new frmRegistration();
                    frmSetting.ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("System error !");
            }
        }

        frmScoring frmScore;
        /// <summary>
        /// Open the score window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmScore_Click(object sender, EventArgs e)
        {
            try
            {
                if (frmScore == null || frmScore.IsDisposed)
                {
                    frmScore = new frmScoring();
                    frmScore.Show();
                }
                else
                {
                    frmScore.Activate();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("System error !");
            }
        }



        /// <summary>
        /// Round reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmRoundReports_Click(object sender, EventArgs e)
        {
            try
            {
                frmRounds displayRounds = new frmRounds();
                displayRounds.Show();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// display rank
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmRankReport_Click(object sender, EventArgs e)
        {
            try
            {
                frmRank displayRank = new frmRank();
                displayRank.Show();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// If this window shutdown normally, set the RecoreModel.IsNornam = true;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                
            }
            catch (Exception)
            {

            }
        }
    }
}
