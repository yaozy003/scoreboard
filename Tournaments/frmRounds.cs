﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tournaments.Extend;
using Tournaments.Models;
using Tournaments.Models.InfoModels;
using Tournaments.Services;

namespace Tournaments
{
    public partial class frmRounds : Form
    {
        public frmRounds()
        {
            InitializeComponent();
        }
        RegistrationModel registration = new RegistrationModel();
        ApiService apiService = new ApiService();
        /// <summary>
        /// Load data from the database and create datatables
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmRounds_Load(object sender, EventArgs e)
        {
            try
            {
                var result = FileHelper.ReadContent("registration.bak");

                //Determine if the tournament exits
                if (string.IsNullOrEmpty(result))
                {
                    MessageBox.Show("Tournament id is null");
                    return;
                }

                registration = result.ToObject<RegistrationModel>();

                if (registration == null || string.IsNullOrEmpty(registration.TouranmentId))
                {
                    MessageBox.Show("Tournament Id is null. Please try again .");
                    return;
                }
                QualifyingModel qualifying = apiService.QueryMatches(registration.TouranmentId, MatchesTypeEnum.qualifying) as QualifyingModel;
                FinalsModel finals = apiService.QueryMatches(registration.TouranmentId, MatchesTypeEnum.finals) as FinalsModel;

                //Insert column into the datatable
                DataTable datas1 = new DataTable();
                DataColumn roundId = new DataColumn("Round Num", typeof(string));
                DataColumn team1 = new DataColumn("Team 1", typeof(string));
                DataColumn team2 = new DataColumn("Team 2", typeof(string));
                DataColumn team1Score = new DataColumn("Team 1 Score", typeof(string));
                DataColumn team2Score = new DataColumn("Team 2 Score", typeof(string));

                datas1.Columns.AddRange(new DataColumn[] { roundId, team1, team2, team1Score, team2Score });

                //Load the qualifying rounds` info
                foreach (var x in qualifying.Rounds)
                {
                    if (x.Teams.Count == 0)
                        continue;
                    DataRow row = datas1.NewRow();
                    row[0] = x.Round;
                    row[1] = x.Teams[0].Name;
                    row[2] = x.Teams[1].Name;
                    row[3] = x.Teams[0].Score;
                    row[4] = x.Teams[1].Score;

                    datas1.Rows.Add(row.ItemArray);
                }

                dgvQualifying.DataSource = datas1;

                //Final rounds info
                DataTable datas2 = new DataTable();
                DataColumn roundId2 = new DataColumn("Round Num", typeof(string));
                DataColumn team12 = new DataColumn("Team 1", typeof(string));
                DataColumn team22 = new DataColumn("Team 2", typeof(string));
                DataColumn team12Score = new DataColumn("Team 1 Score", typeof(string));
                DataColumn team22Score = new DataColumn("Team 2 Score", typeof(string));
                datas2.Columns.AddRange(new DataColumn[] { roundId2, team12, team22, team12Score, team22Score });
                foreach (var x in finals.Rounds)
                {
                    if (x.Teams.Count == 0)
                        continue;
                    DataRow row = datas2.NewRow();
                    row[0] = x.Round;
                    row[1] = x.Teams[0].Name;
                    row[2] = x.Teams[1].Name;
                    row[3] = x.Teams[0].Score;
                    row[4] = x.Teams[1].Score;

                    datas2.Rows.Add(row.ItemArray);
                }

                dgvFinal.DataSource = datas2;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
